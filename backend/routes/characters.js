const express = require('express');
const bodyParser = require('body-parser');

var fetch = require('node-fetch');


const Character = require('../models/characters');

const charactersRouter = express.Router();
charactersRouter.use(bodyParser.json());

charactersRouter.route('/')
.options( (req, res) => {res.sendStatus(200); })
.post(async function (req, res, next) {

    try {
        var isCharacters = await Character.find({}).limit(10)

        if (!isCharacters || isCharacters == '' || isCharacters == undefined){

            const data = await fetch('https://api.got.show/api/book/characters')
            const characters = await data.json()
            await Character.insertMany(characters);
        }
    }    
    catch (e){
            console.log(`there was an error Cod.100 ${e}`)
    }

    if (req.body[0].searchName === undefined || req.body[0].searchHouse === undefined ){
        res.statusCode = 405;
        res.setHeader('Content-Type', 'application/json');
        res.end('debe proporcionar valores para searchName y searchHouse')
    }
    else{
        var name = req.body[0].searchName;
        var house = req.body[0].searchHouse;
    }

    console.log(`valores : ${name} y ${house}`)
    var charactersPerPage = 10;
    var page = req.body[0].page

    switch (name, house){
        case (!name && !house):
            try {
                var displayCursor = await Character.find({}, {name: 1, image: 1}).sort([["name", 1]]).skip(page*charactersPerPage).limit(charactersPerPage)
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(displayCursor);
            }
            catch (e){
                console.log(`There was an error retrieving the character Cod. 101 ${e}`)
            }
            break            
        case name && house:
            try {
                var displayCursor = await Character.find({name: {$regex: name, $options: "i"}, house: {$regex: house,  $options: "i"}}, {name: 1, image: 1}).sort([["name", 1]]).skip(page*charactersPerPage).limit(charactersPerPage)
                console.log(displayCursor)
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(displayCursor);
            }
            catch (e){
                console.log(`There was an error retrieving the character ${e}`)
            }
            break
        case name:
            try {
                var dataChar = await Character.find({ name: {$regex: name,  $options: "i"} }, {name: 1, image: 1}).sort([["name", 1]]).skip(page*charactersPerPage).limit(charactersPerPage)
                console.log(dataChar)
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dataChar);
            }
            catch (e){
                console.log(`There was an error retrieving the character ${e}`)
            }
            break
        case house: 
            try {
                var dataChar = await Character.find({ house: {$regex: house,  $options: "i"} }, {name:1, image: 1}).sort([["name", 1]]).skip(page*charactersPerPage).limit(charactersPerPage)
                console.log(dataChar)
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dataChar);
            }
            catch (e){
                console.log(`There was an error retrieving the character ${e}`)
            }
            break
    }
})

.put((req, res, next) => {
    res.statusCode = 403;
    res.end('put operation not supported');
  })

.delete((req, res, next) => {
res.statusCode = 403;
res.end('delete operation not supported');
})

charactersRouter.route('/:id')
.options((req, res) => {res.sendStatus(200); })
.get( async function (req, res, next) {
    
    try {
        var dataChar = await Character.findOne({ _id: req.params.id })
        console.log(dataChar)
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(dataChar);
    }
    catch (e){
        console.log(`There was an error retrieving the character ${e}`)
    }
})

.post((req, res, next) => {
    res.statusCode = 403;
    res.end('post operation not supported');
    })

    
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('put operation not supported');
    })

.delete((req, res, next) => {
    res.statusCode = 403;
    res.end('delete operation not supported');
    })

module.exports = charactersRouter;