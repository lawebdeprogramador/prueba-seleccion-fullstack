const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var pagerRankSchema = new Schema({
  title: {
    type: String
  },
  rank: {
    type: Number
  }
});

var characterSchema = new Schema({
  titles: {
    type: Array
  },
  spouse: {
    type: Array
  },
  children: {
    type: Array
  },
  allegiance: {
    type: Array
  },
  books: {
    type: Array
  },
  plod: {
    type: Number
  },
  longevity: {
    type: Array
  },
  plodB: {
    type: Number
  },
  plodC: {
    type: Number
  },
  longevityB: {
    type: Array
  },
  longevityC: {
    type: Array
  },
  name: {
    type: String
  },
  slug: {
    type: String
  },
  gender: {
    type: String
  },
  image: {
    type: String
  },
  culture: {
    type: String
  },
  house: {
    type: String
  },
  alive: {
    type: Boolean
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  },
  pagerRank: pagerRankSchema,
  id: {
    type: String
  }
});

const Characters = mongoose.model("Character", characterSchema);

module.exports = Characters;
