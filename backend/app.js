// Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser')

// Inicializar variables
var app = express();

// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

// Body parse
// parse application/x - www - form - urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Importar Rutas
var charactersRoutes = require('./routes/characters');
// Conexión a la base de datos
mongoose.connection.openUri('mongodb://localhost:27017/charactersDB', (err, res) => {
    if (err) throw err;
    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
});

// funcion para llenar BD

// Rutas
app.use('/characters', charactersRoutes);


// Escuchar peticiones
app.listen(5000, () => {
    console.log('Express server puerto 5000: \x1b[32m%s\x1b[0m', 'online');
});